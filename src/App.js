import React from 'react';
import './App.css';
import PageRouter from "./routers/PageRouter";

export default class App extends React.Component {
  render() {
    return(
      <React.Fragment>
        <PageRouter />
      </React.Fragment>
    )
  }
}




