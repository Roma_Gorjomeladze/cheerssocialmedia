import React, { Component } from 'react';
import {isAuthenticated} from "../auth/Auth";
import {read , update} from "../apiuser/apiUser";
import {Redirect} from "react-router-dom";
import defaultProfile from "../../img/images.png"
import './editProfile.css'

export default class editProfile extends Component{
    constructor(){
        super()
        this.state = {
            name: '',
            selectedFile: '',
            email: '',
            password: '',
            loading: false,
            error: '',
            redirectToProfile: false,
            userImgPath: ''
        }


        

    }
    isValid =()=> {
        const {name ,email ,password ,fileSize} = this.state;
        if(fileSize > 100000){
            this.setState({
                error: "File size shoulde be less than 100 kb",
                loading : false
            })
            return false
        }
        if(name.length === 0){
            this.setState({
                error: "Name is required",
                loading :false
            })
            return false
        }
        if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
            this.setState({
                error: "A valid email is required",
                loading: false 
            })
            return false
        }
        if(password.length >=1 && password.length <=5){
            this.setState({
                error: "Password must be at least 6 characters long",
                loading: false
            })
            return false
        }
        return true
    }

    componentDidMount(){
        const userId = this.props.match.params.userId
        const token = isAuthenticated().token;
        read(userId,token)
        .then((data)=>{
           this.setState({
               userImgPath: data.imgPath,
               
           })
        })
    }

    //catch the data from inputs 
    onFileChange = (event)=>{
        this.setState({
            selectedFile: event.target.files[0]
        })
    }
    onNameChange = (event)=>{
        this.setState({
            name: event.target.value
        })
    }
    onEmailChange = (event)=> {
        this.setState({
            email: event.target.value
        })
    }
    onPasswordChange = (event)=>{
        this.setState({
            password: event.target.value
        })
    }
    handleSubmit=(event)=>{
        event.preventDefault()
        const {name, selectedFile, password, email} = this.state
        var formData = new FormData()
        formData.append('name', name)
        formData.append('email', email)
        formData.append('password', password)
        formData.append('selectedFile', selectedFile)
        const userId = this.props.match.params.userId
        const token = isAuthenticated().token;
        update(userId,token,formData)
        this.setState({
            redirectToProfile : true
        })
    }

    render(){
       
        if(this.state.redirectToProfile){
            return <Redirect to ={`/`}/>
        }
        return(
            <div className = 'wrapper'>
                <img className='profileImage' src={`http://localhost:5000/${this.state.userImgPath}`} alt=""/>
                <form className = "userForm" encType='multypart/form-data'>
                    <label className='bmd-label-floating'> Image</label>
                    <input type="file" onChange = {this.onFileChange} name="file" id="file" className="form-control" />  
                    <label className='bmd-label-floating' >name</label>
                    <input type="text" name="name" onChange = {this.onNameChange} className='form-control'/>
                    <label className='bmd-label-floating' >email</label>
                    <input type="text" name="email" onChange={this.onEmailChange} className = 'form-control'/>
                    <label className='bmd-label-floating' >password</label>
                    <input type="password" name="password" onChange ={this.onPasswordChange} className='form-control'/>
                    <input onClick = {this.handleSubmit} className = 'btn btn-raised btn-primary mt-2'type="submit" value='Update'/>
                </form>
            </div>
        )
    }
}

// export default class editProfile extends Component {
//     constructor() {
//         super();
//         this.state = {
//             selectedFile: '',
//             id : "",
//             name : "",
//             email : "",
//             password : "",
//             redirectToProfile : false,
//             error : "" ,
//             loading: false,
//             fileSize : 0
//         }
//     }

//     init =(userId)=> {
//         const token = isAuthenticated().token;
//         read(userId ,token)
//         .then(data => {
//             if(data.error) {
//                 this.setState({
//                     redirectToProfile : true
//                 })
//             }else {
//                 this.setState({
//                     id : data._id,
//                     name : data.name ,
//                     email : data.email ,
//                     error : ""
//                 })
//             }
//         })
//     }

//     componentDidMount () {
//         this.userData = new FormData()
//     const userId = this.props.match.params.userId
//     console.log('this props is ',this.props)
//     this.init(userId)
//     };

//     // isValid =()=> {
//     //     const {name ,email ,password ,fileSize} = this.state;
//     //     if(fileSize > 100000){
//     //         this.setState({
//     //             error: "File size shoulde be less than 100 kb",
//     //             loading : false
//     //         })
//     //         return false
//     //     }
//     //     if(name.length === 0){
//     //         this.setState({
//     //             error: "Name is required",
//     //             loading :false
//     //         })
//     //         return false
//     //     }
//     //     if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
//     //         this.setState({
//     //             error: "A valid email is required",
//     //             loading: false 
//     //         })
//     //         return false
//     //     }
//     //     if(password.length >=1 && password.length <=5){
//     //         this.setState({
//     //             error: "Password must be at least 6 characters long",
//     //             loading: false
//     //         })
//     //         return false
//     //     }
//     //     return true
//     // }

//     handleChange= name => event =>{
//         this.setState({
//             error : ""
//         })
//         const value = name === "photo" ? event.target.files[0] : event.target.value
//         const fileSize = name === "photo" ? event.target.files[0] .size: 0 
//         this.userData.set(name ,value)
//         this.setState({[name]: value ,fileSize})

//        }



//        handleClick =event=> {
//         event.preventDefault();
//         this.setState({
//             loading: true
//         })
//         if(this.isValid()) {


//         const userId = this.props.match.params.userId
//         const token = isAuthenticated().token;
//             let {name, email, password, selectedFile} = this.state
//             var formData = new FormData()
//             formData.append("name", name)
//             formData.append("email", email)
//             formData.append("password", password)
//             formData.append("selectedFile", selectedFile.filename)
//             update(userId , token ,formData)
//             .then(data=> {
//                 if(data.error) this.setState({
                    
//                     error: data.error
                    
//                 })
//                 else this.setState({
//                 redirectToProfile : true
//                 })
//             })
            
//         }
//        }

//     signupForm=(name,email,password)=> (

//         <form encType='multipart/form-data'>
//             <div className='form'>
//                 <div className="wrapper">
//                     <div className="form-group">
//                         <div className="label">
//                             <label className="text-muted"></label>
//                         </div>
//                         <input onChange={this.handleFileChange} type="file" accept="image/*" name='selectedFile' placeholder='Firstname' className='form-control' />
//                     </div>
//                     <div className="form-group">
//                         <div className="label">
//                         <label className="text-muted"></label>

//                         </div>
//                         <input onChange={this.handleNameChange} type="text" placeholder='Firstname' className='form-control' value={name}/>
//                     </div>
//                     <div className="form-group">
//                         <label className="text-muted" ></label>
//                         <input onChange={this.handleEmailChange} type="email" placeholder='Email' className='form-control'  value={email}/> 
//                     </div>
//                     <div className="form-group">
//                         <label className="text-muted"></label>
//                         <input onChange={this.handlePasswordChange} name='password' type="Password" placeholder='Password' className='form-control'  value={password}/> 
//                     </div>
                
//                     <div className="label-input button-signup">
//                         <input type="submit" onClick={this.handleClick} value="Update"/>
//                     </div>
                    
//                 </div>
//             </div>
//         </form>
//     )

    

//     render() {
//         const  {id ,name ,email ,password , redirectToProfile ,error ,loading} = this.state;
//         if(redirectToProfile) {
//             return <Redirect  to = {`/user/${id}`}/>
//         }

//         const photoUrl = id ? `http://localhost:5000/user/photo/${id}?${new Date().getTime()}` : defaultProfile
//         return (
//             <div className="container">
//                 <h2 className="mt-5 mb-5" style={{width: "500px"}}>Edit Profile</h2>
//                 <div
//                     className="alert alert-danger"
//                     style={{ display: error ? "" : "none" }}
//                 >
//                     {error}
//                 </div>
//                 {loading ? (
//                     <div className="jumbotron text-center">
//                         <h2>Loading...</h2>
//                     </div>
//                 ) : (
//                     ""
//                 )}

//                 <img 
//                 className="img-thumnbnail"
//                 style ={{height: "200px" , width: "200px"}}
//                  src={photoUrl} 
//                  onError={i =>(i.target.src = `${defaultProfile}`)} 
//                  alt={name} />

//                 {this.signupForm(name,email,password)}
//             </div>
//         )
//     }
// }
