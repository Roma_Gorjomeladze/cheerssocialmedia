import React from "react";
import Image from "../../img/ezgif.com-crop.gif";

export default class GifComponent extends React.Component {
    render () {
        const background = {
            picture : Image
            }
        return (
            <div className="gif-space">
                <img className="fist-bump" alt="hello world" src={background.picture}/>
                </div>
        )
    }
}