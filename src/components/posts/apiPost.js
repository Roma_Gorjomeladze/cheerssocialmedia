export const create = (post, token, userId) => {
    
    console.log("USER DATA UPDATE: ",post );
    return fetch(`http://localhost:5000/post/new/${userId}`, {
        method: "POST",
        headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json"
        },
        body: post
    })
        .then(response => {
            console.log(response.json())
            return response.json();
        })
        .catch(err => console.log(err));
};



export const list = () => {
    return fetch(`http://localhost:5000/posts`, {
        method: "GET"
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

export const singlePost = (postId) => {
    return fetch(`http://localhost:5000/post/${postId}`, {
        method: "GET"
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};


export const listByUser = (userId , token) => {
    return fetch(`http://localhost:5000/post/by/${userId}`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
        }
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

export const remove =(postId ,token) => {
    return fetch(`http://localhost:5000/post/${postId}`,{
        method :" DELETE",
        headers:{
            Accept: "application/json",
            "Content-Type" : "applicaton/json",
            Authorization :`Bearer ${token}`
        }
    })
    .then(response => {
        return response.json();
    })
    .catch(err => console.log(err))
}

export const like =(userId ,postId ,token) => {
    return fetch(`http://localhost:5000/post/like`,{
        method :" PUT",
        headers:{
            Accept: "application/json",
            "Content-Type" : "applicaton/json",
            Authorization :`Bearer ${token}`
        } ,
        body :JSON.stringify ({userId , postId})
    })
    .then(response => {
        return response.json();
    })
    .catch(err => console.log(err))
}

export const unlike =(userId ,postId ,token) => {
    return fetch(`http://localhost:5000/post/unlike`,{
        method :" PUT",
        headers:{
            Accept: "application/json",
            "Content-Type" : "applicaton/json",
            Authorization :`Bearer ${token}`
        } ,
        body : JSON.stringify({userId , postId})
    })
    .then(response => {
        return response.json();
    })
    .catch(err => console.log(err))
}

export const comment =(userId ,postId ,token ,comment) => {
    return fetch(`http://localhost:5000/post/comment`,{
        method :" PUT",
        headers:{
            Accept: "application/json",
            "Content-Type" : "applicaton/json",
            Authorization :`Bearer ${token}`
        } ,
        body : ({userId , postId ,comment})
    })
    .then(response => {
        return response.json();
    })
    .catch(err => console.log(err))
}
export const uncomment =(userId ,postId ,token ,comment) => {
    return fetch(`http://localhost:5000/post/uncomment`,{
        method :" PUT",
        headers:{
            Accept: "application/json",
            "Content-Type" : "applicaton/json",
            Authorization :`Bearer ${token}`
        } ,
        body : ({userId , postId ,comment})
    })
    .then(response => {
        return response.json();
    })
    .catch(err => console.log(err))
}

