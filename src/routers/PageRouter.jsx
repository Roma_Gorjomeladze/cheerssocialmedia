import React from "react";
// eslint-disable-next-line
import { BrowserRouter as Router, Route,Switch, Link } from "react-router-dom";
import Registration from "../pages/Registration";
import LoginComponent from "../components/Login/LoginComponent";
import MainPage from "../pages/MainPage";
import Menu from "../components/menu/Menu";
import Profile from "../components/profile/Profile";
import UsersComponent from "../components/users/UsersComponent";
import editProfile from "../components/edit-profile/editProfile";
import NewPost from "../components/posts/NewPost";
import SinglePost from "../components/posts/SinglePost";

export default class PageRouter extends React.Component {
    render() {
        return(
            <Router>
                <Menu />
                 <Switch>
                    <Route path="/" exact component={MainPage} />
                    <Route path="/post/:postId" exact component={SinglePost} />
                    <Route path="/users" exact component={UsersComponent} />
                    <Route path="/registration" component={Registration} />
                    <Route path="/signin" component={LoginComponent}/>
                    <Route path="/user/edit/:userId" component={editProfile} />
                    <Route path="/user/:userId" component={Profile} />
                    <Route path="/post/create" component={NewPost} />
                 </Switch>
            </Router>
        )
    }
}