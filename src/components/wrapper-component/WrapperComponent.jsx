import React from "react";
import styled from "styled-components";

const WrapperDiv = styled.div`
display: flex;
width: 100%;
`

export default class WrapperComponent extends React.Component {
    render() {
        return(
            <WrapperDiv>{this.props.children}</WrapperDiv>
        )
    }
}