import React from "react";

export default class TitleComponent extends React.Component {
    
    render(){
              var textStyle = {
              margin: "0 auto",
              textAlign :"center",
              fontFamily: "Segoe UI"
              };
        return(
            <div className="title-space">
                <h1 style={textStyle}>Connect with friends and the
                world around you on <strong>Cheers</strong>.</h1>
                </div>
        )
    }
}