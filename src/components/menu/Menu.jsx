import React from "react";
import {Link ,withRouter} from "react-router-dom";
import {Signout , isAuthenticated} from "../auth/Auth";
import "./menu.css";




const isActive = (history, path) => {
    if(history.location.pathname === path)
    return {color: "#ffa500"}
    else return {color :"#ffffff"}
}


const Menu =({history}) => (
    
    <div >
            <ul className="nav ">
            <Link to ={"/"} className="test1 ">Cheers</Link> 
            <ul className="test2 ">
            <ul className="nav nav-tabs  justify-content-end" style={{fontFamily:"Arial, Helvetica, sans-serif"}}>

        <li className="nav-item">
        <Link className="nav-link" to="/" style={isActive(history, "/")}>Home</Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link" to="/users" style={isActive(history, "/users")}>Users</Link>
        </li>
        {!isAuthenticated() && (
        <>
                <li className="nav-item">
        <Link className="nav-link" to="/registration"style={isActive(history, "/registration")}>Sign Up</Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link" to="/signin"style={isActive(history, "/signin")}>Sign In</Link>
        </li>
        </>
        )}
        {isAuthenticated() && (
        <>
                <li className="nav-item">
        <Link className="nav-link" to='#'
        style={isActive(history, "/signup")} onClick={()=> Signout(()=> history.push("/registration"))}>Sign Out</Link>
        </li>
        <li className="nav-item">
            
                <Link className="nav-link"
                style={isActive(history, `/user/${isAuthenticated().user._id}`)}
                to={`/user/${isAuthenticated().user._id}` }>
                    {`${isAuthenticated().user.name}'s Profile`}
                </Link>

        </li>

        </>
        )}
        </ul>
                    </ul>
                    
                    
                    </ul>
                                    
                    

                

            
    </div>
);

export default withRouter(Menu);