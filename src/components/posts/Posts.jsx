import React, { Component } from 'react'
import {list} from "../body-main/BodyMain";
import defaultProfile from "../../img/images.png"
import {Link} from "react-router-dom";
import {like ,unlike} from "../posts/apiPost"
import "./posts.css"

export default class Posts extends Component {
    constructor(){
        super();
        this.state = {
            posts : [],

        }
    }

   

    renderPosts = posts => {
        return(
            <div className="row"  >
            {posts.map((post ,i)=> {
                const posterId = post.postedBy ? post.postedBy._id : ""
                const posterName = post.postedBy ? post.postedBy.name : " Unknown"
               
               return (<div  className="card col-md-8"  key ={i}>

               <img 
                className="img-thumnbnail"
                style ={{height: "200px" , width: "200px" , margin: "0 auto" , borderRadius: "50%"}}
                src={`http://localhost:5000/${post.imgPath}`}
                 onError={i =>(i.target.src = `${defaultProfile}`)}
                alt={post.name} />
                <div className="card-body">
                  <h5 className="card-title">{post.title}</h5>
                  <p className="card-text">{post.body.substring(0,100)}</p>
                  <br />
                  <p>
                      Posted By {" "}
                      <Link to ={`/user/${posterId}`}>
                          {posterName} {" "}
                      </Link>
                       On { new Date(post.created).toDateString()}
                  </p>
                  <Link 
                  to ={`/post/${post._id}`}
                   className="btn btn-raised btn-primary btn-sm">
                       Read more
                       </Link>
                       
                </div>
                
              </div>
            )})}
        </div>
        )
    }

    render() {
        const {value} = this.props;
        return (
            <div className="container">
                <h2 className="mt-5 mb-5">Recent Posts</h2>
                {this.renderPosts(value)}
            </div>
        )
    }
}
