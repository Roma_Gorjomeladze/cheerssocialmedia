import React from  'react'
import CommentsComponent from './comments/CommentsComponent'
import PostServices from '../../../services/usersService/PostsServices'
export default class PostComponent extends React.Component{
    constructor(props){
        super(props);
        this.postServices = new PostServices()
        this.state = {
            objects: []
        }
    }


    componentDidMount(){
        this.postServices.getPosts()
        .then(res =>{
            this.setState({
                objects: res
            })
            console.log(this.state)
        })
        
    }
    render(){
        return(
            <React.Fragment>
                <div>
                    <header>
                        <span>{this.props.author}</span>
                        <span>{this.props.title}</span>   
                    </header>
                    <div>
                        <p>{this.props.bodyText}</p>
                    </div>
                    <div><span>{this.props.likes}</span></div>
                    <div>
                        <CommentsComponent/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}