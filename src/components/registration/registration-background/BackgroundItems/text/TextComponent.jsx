import React from "react";
import styled from "../../../../../../node_modules/styled-components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImages  ,faShareAltSquare ,faSearchPlus} from '@fortawesome/free-solid-svg-icons';

const StyledP = styled.p`
padding: 15px;
font-family :Segoe UI;
font-weight: 600`

const IconSpace = styled.div`
display: flex;
align-items: center`

export default class TextComponent extends React.Component {
    render() {
        return(
            <div className="text-space">
               <IconSpace>
                   <FontAwesomeIcon icon={faImages} style={{opacity: "0.6", fontSize: "25px" ,marginTop: "-12px"}}/>
                    <StyledP>See photos and updates from friends in News Feed.</StyledP>
                 </IconSpace> 
                <IconSpace>
                        <FontAwesomeIcon icon={faShareAltSquare} style={{opacity: "0.6", fontSize: "27px" ,marginTop: "-12px"}}/>
                       <StyledP>Share what's new in your life on your Timeline.</StyledP>
                </IconSpace>
                <IconSpace>
                        <FontAwesomeIcon icon={faSearchPlus} style={{opacity: "0.6", fontSize: "25px", marginTop: "-10px"}}/>
                        <StyledP>Find more of what you're looking for with Cheers Search.</StyledP>
                </IconSpace>
            </div>
        )
    }
}