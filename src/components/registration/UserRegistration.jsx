import React from "react";
import "./UserRegistration.css";
import RegistrationMain from "./registration-main/RegistrationMain";
export default class UserRegistration extends React.Component {
    render() {
        return (
            <div className="registration-space">
                
                <RegistrationMain /> 
            </div>
        )
    }
}