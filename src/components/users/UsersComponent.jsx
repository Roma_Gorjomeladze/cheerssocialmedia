import React, { Component } from 'react'
import {list} from "../apiuser/apiUser";
import defaultProfile from "../../img/images.png"
import {Link} from "react-router-dom";

export default class UsersComponent extends Component {
    constructor(){
        super();
        this.state = {
            users : []
        }
    }

    componentDidMount() {
        list().then(data => {
            if(data.error) {
                console.log(data.error)
            }else {
                this.setState({
                    users: data
                })
            }
            console.log(this.state.users)
        })
    }

    renderUsers= (users)=> (
        <div className="row" >
                    {users.map((user ,i)=> (
                        <div className="card col-md-3" style={{margin: "5px"}}  key ={i}>
                       <img 
                        className="img-thumnbnail"
                        style ={{height: "200px" , width: "200px" , margin : "0 auto", borderRadius: "50%"}}
                
                        src={`http://localhost:5000/${user.imgPath}`}
                        alt={user.name} />
                        <div className="card-body">
                          <h5 className="card-title"
                          style={{color: "red"}}>{user.name}</h5>
                          <p className="card-text">{user.email}</p>
                          <Link 
                          to ={`/user/${user._id}`}
                           className="btn btn-raised btn-primary btn-sm" style={{width: "110px"}}>
                               View Profile
                               </Link>


                            
                        </div>
                      </div>
                    ))}
                </div>
    )

    render() {
        const {users} = this.state;
        return (
            <div className="container">
                <h2 className="mt-5 mb-5">Users</h2>
                {this.renderUsers(users)}
            </div>
        )
    }
}
