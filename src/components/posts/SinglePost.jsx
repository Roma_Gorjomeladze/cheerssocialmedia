import React, { Component } from 'react';
import {singlePost , remove , like , unlike} from "./apiPost";
import defaultProfile from "../../img/images.png"
import {Link , Redirect} from "react-router-dom";
import { isAuthenticated } from '../auth/Auth';
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Comment from "../comments/comment"

export default class SinglePost extends Component {
    state = {
        post :"",
        redirectToHome : false , 
        like : false,
        likes : 0,
        comments : []
    }

    checkLike=(likes)=> {
        const userId = isAuthenticated().user._id;
        let match = likes.indexOf(userId) !== -1;
        return match;
    }

    componentDidMount=()=>{ 
        const postId = this.props.match.params.postId
        singlePost(postId)
        .then(data => {
            if(data.error){
                console.log(data.error)
            }else {
                this.setState({
                    post : data ,
                    like : this.checkLike(data.likes),
                    likes : data.likes.length , 
                    comments : data.comments
                })
            }
        })
    }

    updateComments =comments=> {
        this.setState({
            comments
        })
    }

    likeToggle=()=> {
        let callApi = this.state.like ? unlike : like
        const userId = isAuthenticated().user._id
        const postId = this.state.post._id
        const token = isAuthenticated().token

        callApi(userId , postId , token)
        .then(data => {
            if(data.error){
                console.log(data.error)
            }else {
                this.setState({
                    like : !this.state.like,
                    likes : data.likes.length
                })
            }
        })
    }
    deletePost=()=> {
        const  postId = this.props.match.params.postId;
        const token = isAuthenticated().token;
        remove(postId , token)
        .then(data => {
            if(data.error){
                console.log(data.error)
            }else {
                this.setState({redirectToHome: true})
            }
        })
    }
    deleteConfirmed =()=> {
        let answer = window.confirm("Are you sure you want to delete your account?")
        if(answer) {
            this.deletePost()
        }
    }

    renderPost =(post ) => {
        const posterId = post.postedBy ? post.postedBy._id : ""
        const posterName = post.postedBy ? post.postedBy.name : " Unknown"

        const {like ,likes } = this.state;
        return (
       <div className="card-body">
        <img 
         className="img-thumnbnail"
         style ={{height: "300px" , width: "100%" , margin: "0 auto",objectFit: "cover"}}
         src={`http://localhost:5000/posts/${post._id}`}
          onError={i =>(i.target.src = `${defaultProfile}`)}
         alt={post.name} />
         <div >
             <div>
            
                 {like ? (
                     <h3 onClick={this.likeToggle}>  <FontAwesomeIcon icon={faThumbsUp} />{" "}{likes} Like</h3>
                 ) : (
                    <h3 onClick={this.likeToggle}> <FontAwesomeIcon icon={faThumbsUp} />{" "}{likes} Like</h3>
                    
                 ) }


             </div>
             <h2 className="display-2 mt-5 mb-5">{post.title}</h2>
           <h3 className="card-text" style={{  fontWeight: "bold"}}>{post.body}</h3>
           <br />
           <p >
               Posted By {" "}
               <Link to ={`/user/${posterId}`}>
                   {posterName} {" "}
               </Link>
                On { new Date(post.created).toDateString()}
           </p>
           <Link 
           to ={`/`}
            className="btn btn-raised btn-primary btn-sm" style={{width: "120px" ,height: "35px"}}>
                BACK 
                </Link>
                {
                    isAuthenticated().user &&
                    isAuthenticated().user._id ===post.postedBy._id && (
                       
                        <button onClick ={this.deleteConfirmed} className="btn btn-raised btn-danger" style={{width: "120px" , marginLeft : "10px"}}>
                        Delete Post
                        </button>
                       
                    )
                }
         </div>

       </div>
     

       )
    }
    render() {

        const {post ,comments} = this.state; 

        if(this.state.redirectToHome) {
            return <Redirect to={`/`} />
        }
        
        return (
            
            <div className="container">
                {!post ?  (
                    <div className="jumbotron text-center">
                        <h2>Loading...</h2>
                    </div>
                ) :(
                    this.renderPost(post)
                )}
                <Comment postId ={post._id} comments ={comments.reverse()} updateComments={this.updateComments}/>
                
            
                
            </div>
        )
    }
}
