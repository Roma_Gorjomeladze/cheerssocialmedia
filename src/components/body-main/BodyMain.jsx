import React from "react";
import Posts from "../posts/Posts";
import WrapperComponent from "../wrapper-component/WrapperComponent";
import styled from "styled-components";
import "./BodyMain.css";
import {list} from "../posts/apiPost"
import {recUsers} from "../apiuser/apiUser"
import NewPost from "../posts/NewPost";

const MainDiv = styled.div`
display : flex;
width: 95%;
flex-direction: column;
margin: 0 auto;
background: #f2f2f2f2;
`





     
    

    export default class BodyMain extends React.Component {
        constructor(){
            super();
            this.state ={
                posts : [] ,
                recUsers : []
            }

            this.onUpdatePost = this.onUpdatePost.bind(this);

        }

        componentDidMount(){
            list().then(data => {
                if(data.error) {
                    console.log(data.error)
                }else {
                    this.setState({
                        posts: data
                    })
                }
            })
            this.ShowRecUsers()
        }

        onUpdatePost() {
            console.log('sdsdsds')
            list().then(data => {
                if(data.error) {
                    console.log(data.error)
                }else {
                    this.setState({
                        posts: data
                    })
                }
            })
        }

        ShowRecUsers() {
            recUsers().then(data => {
                console.log("aaaaaaad",data)
                if(data.error) {
                    console.log(data.error)
                }else {
                    this.setState({
                        recUsers: data
                    })
                }
                console.log('recomended users', this.state.recUsers)
            })
        }

        render(){
            const recUsers = this.state.recUsers;
            const users = recUsers.map(show => {
                console.log('show is ',show)
                 return(
                    
                 <div className="wrap">
                      <div> <img  style={{borderRadius: "50%" , marginTop: "5px" , marginRight:"15px"}}className='profileImg' src={`http://localhost:5000/${show.imgPath}`} alt="profile"/>
                      </div>
                      <div>
                    <div className="recUsersSpace"><span>Name: {show.name} </span>
                    <hr/>
                    
                 </div>
                 <div> 
                    <span>Followers : {show.followers.length}
                    </span> 
         
                   
                  </div>
                  <div>
                  <span> Following: {show.following.length} 
                  </span>

                  </div>
                  </div>
                 </div>
                 
                 )
             })
             return(
        <MainDiv>
        <div className="jumbotron" >
            <NewPost updatePost={this.onUpdatePost} />
        </div>
        <div>
        <div className="rec-users">
          {users}
        </div>
        <WrapperComponent>
            <Posts value ={this.state.posts}/>
            
            </WrapperComponent>
            
        </div>
        </MainDiv>
            )
        }
    }
