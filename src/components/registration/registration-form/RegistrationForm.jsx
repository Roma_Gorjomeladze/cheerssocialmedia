import React from "react";
import {signup} from  "../../auth/Auth";
import './RegistrationForm.css';
import {Link} from "react-router-dom";

export default class RegistrationForm extends React.Component {
    
    constructor(){
    super();
    this.state = {
        name : "",
        lastname : "" ,
        email :"",
        password :"",
        error : "",
        open: false,
        birthdate: "",
        showradio : true
    }
    }

    radioChange =()=> {
        this.setState({
            showradio : !this.state.showradio
        })
    }

        handleChange= name => event =>{
         this.setState({
             error: ""
         })
         this.setState({[name]: event.target.value})

        }

        handleClick =event=> {
        event.preventDefault();
        const{name,lastname,email,password,birthdate,showradio} =this.state;
        const user = {
            name,
            lastname,
            email,
            password,
            birthdate,
            showradio
        }
        signup(user)
        .then(data=> {
            if(data.error) this.setState({
                
                error: data.error
                
            })
            else this.setState({
                name : "",
                lastname : "" ,
                open : true,
                email :"",
                password :"",
                error : data.error,
                message : data.message,
                birthdate: "",
                showradio : true
            })
        })
        
        }

    

        signupForm=(name,lastname,email,password,birthdate,showradio)=> (
            <div className='form'>
            <div className="wrapper">
                <div className="form-group">
                    <div className="label">
                    <label className="text-muted"></label>

                    </div>
                    <input onChange={this.handleChange("name")} type="text" placeholder='Firstname' className='form-control' value={name}/>
                </div>
                <div className="form-group">
                    <label className="text-muted"></label>
                    <input onChange={this.handleChange("lastname")} type="text" placeholder='Lastname' className='form-control'  value={lastname} />
                </div>
                <div className="form-group">
                    <label className="text-muted" ></label>
                    <input onChange={this.handleChange("email")} type="email" placeholder='Email' className='form-control'  value={email}/> 
                </div>
                <div className="form-group">
                    <label className="text-muted"></label>
                    <input onChange={this.handleChange("password")} type="Password" placeholder='Password' className='form-control'  value={password}/> 
                </div>
                <div className="form-group">

                    <label className="text-muted"></label>
                    <input onChange={this.handleChange("birthdate")} type="Date" className='form-control'  value={birthdate}/>   
                </div>
                <div className="form-group">
                    <label className="text-muted" ></label>
                <div className="radio">
                <label className="btn btn-secondary active">
                    <input onChange={this.handleChange("gendermale")} type="radio"  
                      value={showradio} 
                     style={{marginRight: "10px"}}
                     onChange={this.radioChange}
                     checked={showradio}/>
                    Male
                </label>
                <label className="btn btn-secondary active">
                    <input onChange={this.handleChange("genderfemale")}type="radio" 
                      value={showradio}
                     style={{marginRight: "10px"}}
                     onChange={this.radioChange}
                     checked={!showradio}/>
                    Female
                </label>
                </div>
                </div>
                <div className="label-input button-signup">
                    <button className="btn btn-raised btn-primary" style={{width: "200px"}}>Sign Up With Google</button>
                    <button onClick={this.handleClick} className="btn btn-raised btn-primary" style={{marginTop: "10px"}}>Sign Up</button>
                </div>
                
            </div>
            
        </div>
        )
        

        
    
    render() {
        const{name,lastname,email,password,birthdate,showradio,error ,open} =this.state;
        return(
            <div className='registration-form'>

                
                <div
                    className="alert alert-danger"
                    style={{ display: error ? "" : "none" }}
                >
                    {error}
                </div>

                <div
                    className="alert alert-info"
                    style={{ display: open ? "" : "none" }}
                >
                    New account is successfully created. please {""}
                    <Link to="/signin">Sign in</Link>
                    
                </div>


               {this.signupForm(name,lastname,email,password,birthdate,showradio)}
            </div>
        )
    }
}