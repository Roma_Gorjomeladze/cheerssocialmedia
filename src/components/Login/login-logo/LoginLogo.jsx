import React from "react";
import styled from "styled-components";

const StyledH1 = styled.h1`
background: -webkit-linear-gradient(#F3D250, #F78888 , #5DA2D5);
-webkit-background-clip: text;
-webkit-text-fill-color: transparent;
font-family: "Comic Sans MS", cursive, sans-serif;
font-size : 35px;`

export default class LoginLogo extends React.Component {
    render() {
        return(
            <React.Fragment>
                 <StyledH1>Cheers</StyledH1>
            </React.Fragment>
        )
    }
}