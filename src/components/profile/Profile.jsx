import React, {Component} from "react";
import {isAuthenticated} from "../auth/Auth";
import {Redirect , Link} from "react-router-dom";
import {read} from  "../apiuser/apiUser";
import DeleteUser from "../delete/deleteUser";
import defaultProfile from "../../img/images.png";
import FollowProfileButton from "../follow-profile/FollowProfileButton";
import ProfileTabs from "../profile-tabs/ProfileTabs";
import {listByUser} from "../posts/apiPost";


class Profile extends Component {
    constructor(){
        super();
        this.state = {
            user : { 
                following: [],
                followers : [] 
            },
            redirectToSignIn : false ,
            following : false , 
            error : "",
            posts : []
        }
    };
    // follow 
    checkFollow = user => {
        const jwt = isAuthenticated()
        const match = user.followers.find(follower => {
            return follower._id === jwt.user._id
        })
        return match
    }

    clickFollowButton = callApi => {
        const userId = isAuthenticated().user._id;
        const token = isAuthenticated().token;
        callApi(userId ,token , this.state.user._id)
        .then(data=> {
            if(data.error){
                this.setState({
                    error: data.error
                })
            } else {
                this.setState({
                    user :data ,
                    following : !this.state.following
                })
            }
        })
    }
   

    init =userId=> {
        const token = isAuthenticated().token;
        read(userId ,token)
        .then(data => {
            if(data.error) {
                this.setState({
                    redirectToSignIn : true
                })
            }else {
                let following = this.checkFollow(data)
                this.setState({
                    user: data , 
                    following
                })
                this.loadPosts(data._id)
            }
        })
    }
    
    loadPosts = userId => {
        // const token = isAuthenticated().token;
        // listByUser(userId,token)
        // .then(data => {
        //     if(data.error){
        //         console.log(data.error)
        //     }else {
        //         this.setState({
        //             posts : data
        //         })
        //     }
        // })
    }

    componentDidMount () {
    const userId = this.props.match.params.userId
    this.init(userId)
    };
    componentWillReceiveProps (props) {
        const userId = props.match.params.userId
        this.init(userId)
    
        };
    render() {

        const {redirectToSignIn ,user ,posts} = this.state;

        if(redirectToSignIn){
            return <Redirect to="/signin" />
        }
        const photoUrl = user._id ? `http://localhost:5000/${user.imgPath}?${new Date().getTime()}` : defaultProfile

        
        return(
            <div className="container">
                <h2 className="mt-5 mb-5">Profile</h2>
                <div className="row">
                <div className="col md-6">
                <img 
                className="img-thumnbnail"
                style ={{height: "200px" , width: "200px" , borderRadius: "50%"}}
                 src={photoUrl}
                 onError={i =>(i.target.src = `${defaultProfile}`)} 
                 alt={user.name} />

                </div>
                <div className="col-md-6">

                <div className="lead mt-2">
                <p>Hello:{user.name}</p>
                <p>Email:{user.email}</p>
                <p>{`Joined ${new Date(user.created).toDateString()}`}</p>
                </div>

                    {isAuthenticated().user &&
                         isAuthenticated().user._id  === user._id ? (
                             <div className="d-inline-block">
                                 <Link 
                                 className="btn btn-raised btn-primary mr-5"
                                 style={{width: "150px"}}
                                 to={`/user/edit/${user._id}`}>
                                     Edit Profile
                                 </Link>
                                 <DeleteUser userId={user._id} />
                             </div>
                         ) :(<FollowProfileButton 
                         following={this.state.following}
                         onButtonClick ={this.clickFollowButton}
                          />
                          )}
                          
                </div>
                </div>
                <div className="row">
                            <div className="col md-12 mt-5 mb-5">
                            <ProfileTabs
                           followers={user.followers}
                           following={user.following}
                           posts ={posts} />
                            </div>
                </div>
            </div>
        )
    }
}


export default Profile;