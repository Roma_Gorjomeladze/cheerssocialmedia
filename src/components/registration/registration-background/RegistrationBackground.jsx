import React from "react";
import "./RegistrationBackground.css";
import TitleComponent from "./BackgroundItems/title/TitleComponent";
import GifComponent from "./BackgroundItems/gif/GifComponent";
import TextComponent from "./BackgroundItems/text/TextComponent";

export default class RegistrationBackground extends React.Component {
    render () {
        return(
            <div className="registration-background">
                <TitleComponent />
                <GifComponent />
                <TextComponent />
            </div>
        )
    }
}