import React from "react";
import RegistrationBackground from "../registration-background/RegistrationBackground";
import RegistrationForm from "../registration-form/RegistrationForm";
import "./RegistrationMain.css"

export default class RegistrationMain extends React.Component {
    
    render() {
        return(
            <React.Fragment>
                <div className="registration-main">
                    <RegistrationBackground />
                    <RegistrationForm />
                </div>
            </React.Fragment>
        )
    }

}