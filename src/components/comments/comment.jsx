import React, { Component } from 'react';
import {comment ,uncomment} from "../posts/apiPost";
import {isAuthenticated} from "../auth/Auth";
import {Link} from "react-router-dom";
import defaultProfile from "../../img/images.png"

export default class Comment extends Component {

    state = {
        text :""
    }

    handleChange=(event)=> {
        this.setState({
            text : event.target.value
        })
    }

    addComment =e=> {
        e.preventDefault();
        const userId = isAuthenticated().user._id;
        const token = isAuthenticated().token;
        const postId = this.props.postId;



        comment(userId , token , postId ,{text : this.state.text})
        .then(data=> {
            if(data.error){
                console.log(data.error)
            }else {
                this.setState({
                    text : ""
                })
                this.props.updateComments(data.comments)
            }
        })
    }
    render() {
        const {comments} = this.props;
        return (
            <div>
                <h2 className="mt-5 mb-5">Leave a comment</h2>
                <form onSubmit={this.addComment}>
                    <div className="form-group">
                    <input type="text" onChange={this.handleChange} className="form-control" value={this.state.text}
                    placeholder={"Leave a comment..."}/>
                    <button className="btn btn-raised btn-primary mt-2">Post</button>
                    </div>
                </form>
                <hr />
                <div className="col-md-12" >
                        <h3 className="text-primary">
                            {comments.length} Comments
                        </h3>
                        <hr />
                        {comments.map((comment, i) => (
                            <div key={i}>
                                <div>
                                    <Link to={`/user/${comment.postedBy._id}`}>
                                        <img
                                            style={{
                                                borderRadius: "50%",
                                                border: "1px solid black"
                                            }}
                                            className="float-left mr-2"
                                            height="30px"
                                            width="30px"
                                            onError={i =>
                                                (i.target.src = `${defaultProfile}`)
                                            }
                                            src={`http://localhost:5000/user/photo/${comment.postedBy._id}`}
                                            alt={comment.postedBy.name}
                                        />
                                        </Link>
                                        <div>
                                            <p className="lead">
                                                {comment.text}
                                            </p>
                                            <br />
                                                <p >
                                                    Posted By {" "}
                                                    <Link to ={`/user/${comment.postedBy._id}`}>
                                                        {comment.postedBy.name} {" "}
                                                    </Link>
                                                        On { new Date(comment.created).toDateString()}
                                                </p>
                                        </div>
                                    
                                </div>
                            </div>
                        ))}
                    </div>
            </div>
        )
    }
}
