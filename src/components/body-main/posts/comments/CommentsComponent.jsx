import React from  'react'

export default class CommentsComponent extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <React.Fragment>
                <ul>
                    <li>{this.props.comments}</li>
                </ul>
            </React.Fragment>
        )
    }
}